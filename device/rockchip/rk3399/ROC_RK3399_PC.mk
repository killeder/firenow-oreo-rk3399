#
# Copyright 2014 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)
include device/rockchip/rk3399/ROC_RK3399_PC/BoardConfig.mk
# Inherit from those products. Most specific first.
$(call inherit-product, device/rockchip/rk3399/device.mk)
$(call inherit-product, device/rockchip/common/device.mk)

PRODUCT_CHARACTERISTICS := tablet
PRODUCT_SHIPPING_API_LEVEL :=26

PRODUCT_NAME := ROC_RK3399_PC
PRODUCT_DEVICE := ROC_RK3399_PC
PRODUCT_BRAND := Android
PRODUCT_MODEL := ROC_RK3399_PC
PRODUCT_MANUFACTURER := Firefly 
PRODUCT_AAPT_PREF_CONFIG := hdpi

TARGET_BOARD_PLATFORM_PRODUCT := tablet

#PRODUCT_PACKAGES += SoundRecorder

# Get the long list of APNs
PRODUCT_COPY_FILES += vendor/rockchip/common/phone/etc/apns-full-conf.xml:system/etc/apns-conf.xml
PRODUCT_COPY_FILES += vendor/rockchip/common/phone/etc/spn-conf.xml:system/etc/spn-conf.xml
PRODUCT_PROPERTY_OVERRIDES += \
    sys.hwc.device.primary = HDMI-A \
    ro.product.version = 1.0.0 \
    ro.product.ota.host = www.rockchip.com:2300 \
    ro.sf.lcd_density=240


PRODUCT_COPY_FILES += device/rockchip/rk3399/ROC_RK3399_PC/fstab.rk30board.sd:root/fstab.rk30board.sd 
PRODUCT_COPY_FILES += device/rockchip/rk3399/ROC_RK3399_PC/init.rk30board.bootmode.sd.rc:root/init.rk30board.bootmode.sd.rc


#PRODUCT_HAVE_OPTEE := true
